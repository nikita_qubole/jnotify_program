import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.WebSocketAdapter;
import org.eclipse.jetty.websocket.client.ClientUpgradeRequest;
import org.eclipse.jetty.websocket.client.WebSocketClient;
import java.net.URI;

public class ClientProg extends WebSocketAdapter {

    public static Session clientSession = null;

    @Override
    public void onWebSocketConnect(Session sess) {
        super.onWebSocketConnect(sess);
        clientSession = sess;
    }

    @Override
    public void onWebSocketText(String message) {
        super.onWebSocketText(message);
        System.out.println(message);
    }

    @Override
    public Session getSession() {
        return super.getSession();
    }

    @Override
    public void onWebSocketClose(int statusCode, String reason) {
        super.onWebSocketClose(statusCode, reason);
    }


    public static void main(String args[]) throws Exception {


        String uri      = "ws://localhost:" + args[0];  //here args[0] should be port number and args[1] should be filePath that needs to be watched!!
        String filePath = args[1];

        WebSocketClient client = new WebSocketClient();
        client.start();
        URI destinationPathURI = new URI(uri);
        ClientUpgradeRequest request = new ClientUpgradeRequest();
        client.connect(new ClientProg(),destinationPathURI,request).get();
        if(clientSession.isOpen()){
            clientSession.getRemote().sendString(String.format(filePath));
            Thread.sleep(10000000);
        }

    }
}
