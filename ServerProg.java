import net.contentobjects.jnotify.JNotifyException;
import net.contentobjects.jnotify.JNotifyListener;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.websocket.api.Session;
import net.contentobjects.jnotify.JNotify;
import org.eclipse.jetty.websocket.api.UpgradeRequest;
import org.eclipse.jetty.websocket.api.UpgradeResponse;
import org.eclipse.jetty.websocket.api.WebSocketAdapter;
import org.eclipse.jetty.websocket.server.WebSocketHandler;
import org.eclipse.jetty.websocket.servlet.WebSocketCreator;
import org.eclipse.jetty.websocket.servlet.WebSocketServletFactory;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.ConcurrentModificationException;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;


/*
* whenever changes occur in watchedFile, this class is called.
* this class implements JNotifyListener class, which has methods for different events.
* In our case: we need only fileModified method.
* */
class Listener implements JNotifyListener{
    String watchedFileName;
    FileWatchWebsocketHelper helper;

    public Listener(String watchedFileName, FileWatchWebsocketHelper h) {
        this.watchedFileName = watchedFileName;
        this.helper         = h;
    }

    public void fileModified(int wd, String rootPath, String name) {
        helper.sendUpdates(rootPath);
    }

    public void fileCreated(int wd, String rootPath, String name) {
        //do nothing
    }

    public void fileDeleted(int wd, String rootPath, String name) {
        //do nothing
    }

    public void fileRenamed(int wd, String rootPath, String oldName, String newName) {
        //do nothing
    }
}



/*
* This thread class is to add watch on a particular file and notify if any changes occur on the same file
* if any changes occur, JNotify.addWatch calls Listener class
* */
class ThreadFileWatcher implements Runnable{

    String watchedFileName;

    public ThreadFileWatcher(String watchedFile) {
    this.watchedFileName = watchedFile;
    }

    public void run() {
        FileWatchWebsocketHelper helper = new FileWatchWebsocketHelper(watchedFileName);
        helper.listenFileChange();
    }
}



/*
* This class has 3 methods:
* listenFileChange() methods is used to add watch on file, it waits indefinitely for changes, whenever chnages occur, Listener class is called.
* sendupdates() and sendMessages() : these two methods are used to send updated content to the concerned clients.
* */

class FileWatchWebsocketHelper{

    String watchedFileName;                 /* path of the file */
    Session session;
    final int mask = JNotify.FILE_MODIFIED; /* mask is used to put flag-> for which action, trigger event(delete/modify/rename/create). */

    FileWatchWebsocketHelper(String watchedFileName) {
        this.watchedFileName = watchedFileName;
    }

    public void listenFileChange()  {
        try {
            JNotify.addWatch(watchedFileName,mask,false,new Listener(watchedFileName,this));
            while(true){
                /* waiting for any changes indefinitely */
                Thread.sleep(60000);
            }
        }catch (JNotifyException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    public void sendUpdates(String watchedFileName) {
        List<JNotifyMapper> allClientSessions;

        allClientSessions = WebsocketConnectionsMap.websocketConnections(watchedFileName);
        for(JNotifyMapper object : allClientSessions){
            try {
                sendMessage(object);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void sendMessage(JNotifyMapper object) throws IOException {
        BufferedReader buffReader = object.filePointer;
        String line;
        while ((line = buffReader.readLine()) != null ){
            try {
                object.session.getRemote().sendString(line);
            }catch (NullPointerException e){
                System.out.println("Error!!! :" + e);
            }
            catch (IOException e) {
                try {
                    WebsocketConnectionsMap.removeWebsocketConnection(watchedFileName, object);
                }catch (ConcurrentModificationException conModException){
                    System.out.println("ERROR!! Concurrency issue.");
                }
                object = null;

            }
        }
        if(object != null)
            object.filePointer = buffReader;
    }


}


/*
* when any client asks for the file --
* if there is already some content ->  send to client
* or whenever changes occur, only updated content send to the client.
* ----
* following thread class is to send all the contents to client which is already in file when client asks for it.
* */
class ThreadToSendOldContent implements Runnable{

    private JNotifyMapper object;
    private FileWatchWebsocketHelper helper;

    ThreadToSendOldContent(JNotifyMapper object,FileWatchWebsocketHelper helper){
        this.object = object;
        this.helper = helper;
    }

    public void run() {
        try {
            helper.sendMessage(object);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

/* JNotifyMapper class is used to bind FileName, session ID and filePointer in a object*/
class JNotifyMapper {
    public String watchedFileName;
    public Session session;
    BufferedReader filePointer;

    JNotifyMapper(String watchedFileName,Session session,BufferedReader filePointer){
        this.watchedFileName = watchedFileName;
        this.session = session;
        this.filePointer = filePointer;
    }

}



/*
* This is main class
* when ever client tries to connect server,onWebSocketConnect() method is called and it provides a session for information exchange.
* when client sends any text, onWebSocketText() is called, it receives content sent by client.
* when client disconnects the connection,onWebSocketClose() is called.
* ConcurrentHashMap and CopyOnWriteArrayList are thread Safe Data structure
* **/
class WebsocketConnectionsMap {

    public static CopyOnWriteArrayList<JNotifyMapper> websocketConnections(String fileName) {
        return ServerProg.fileMappedWithSessionMap.get(fileName);
    }

    public static void addWebsocketConnection(String fileName, JNotifyMapper mapper) {
        ServerProg.fileMappedWithSessionMap.get(fileName).add(mapper);
    }

    public static  void removeWebsocketConnection(String fileName, JNotifyMapper mapper) {
        ServerProg.fileMappedWithSessionMap.get(fileName).remove(mapper);
    }
}

public class ServerProg extends WebSocketAdapter {

    public Session session;
    public static ConcurrentHashMap<String,CopyOnWriteArrayList<JNotifyMapper>> fileMappedWithSessionMap = new ConcurrentHashMap<String, CopyOnWriteArrayList<JNotifyMapper>>();

    @Override
    public void onWebSocketConnect(Session sess) {  //gets hit when client tries to connect to it
        super.onWebSocketConnect(sess);
        session = sess;
    }

    @Override
    public void onWebSocketText(String fileToBeWatched) { /* hits when any message received from the client side */
        super.onWebSocketText(fileToBeWatched);
        BufferedReader filePointer = null;
        try {
            filePointer = new BufferedReader(new FileReader(fileToBeWatched));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        /* this object is to map fileName ,sessionId and filePointer */
        JNotifyMapper objJnotifyMapperClass = new JNotifyMapper(fileToBeWatched,session,filePointer);

        /*BEGIN --> To send whole content of file to client*/
        FileWatchWebsocketHelper helper = new FileWatchWebsocketHelper(fileToBeWatched);
        Thread objToSendOldContent = new Thread(new ThreadToSendOldContent(objJnotifyMapperClass,helper));
        objToSendOldContent.start();
        /*END*/


        /* if file is already being watched then create object and put an entry on map
         * else
         * call a new thread to add JNotifyWatch on that file*/
        if(fileMappedWithSessionMap.get(fileToBeWatched) != null){
            WebsocketConnectionsMap.addWebsocketConnection(fileToBeWatched,objJnotifyMapperClass);
        }
        else {
            fileMappedWithSessionMap.put(fileToBeWatched,new CopyOnWriteArrayList<JNotifyMapper>());
            WebsocketConnectionsMap.addWebsocketConnection(fileToBeWatched,objJnotifyMapperClass);
            Thread thread = new Thread(new ThreadFileWatcher(fileToBeWatched));
            thread.start();
            System.out.println(fileToBeWatched);
        }

    }

    @Override
    public void onWebSocketClose(int statusCode, String reason) {
        super.onWebSocketClose(statusCode, reason);
    }


    public static void main(String args[]) throws Exception {
        int port = Integer.parseInt(args[0]);
        WebSocketHandler wsHandler = new WebSocketHandler() {
            @Override
            public void configure(WebSocketServletFactory factory) {
                factory.setCreator(new WebSocketCreator() {
                    public Object createWebSocket(UpgradeRequest req, UpgradeResponse resp) {
                        return new ServerProg();
                    }
                });
            }
        };
        Server server = new Server(new InetSocketAddress(port));
        server.setHandler(wsHandler);
        server.start();
    }
}
